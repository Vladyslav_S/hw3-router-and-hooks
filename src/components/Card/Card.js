import React from "react";
import "./Card.scss";
import { star } from "../../themes/icons";
import Button from "../Buttom/Button";
import PropTypes from "prop-types";

const Card = (props) => {
  const {
    product,
    saveCurrentProduct,
    openModal,
    toggleFavorite,
    toCart,
    fromCart,
    isFavorite,
  } = props;
  const { name, picPath, color, articul } = product;

  return (
    <div className="card-container" style={{ backgroundColor: color }}>
      <img src={picPath} alt="Album face" />
      <h4>{name}</h4>
      <p>Prise: 100500 $</p>
      {toCart && (
        <Button
          className="card-add-button"
          onClick={() => {
            saveCurrentProduct(product);
            openModal("Add to cart");
          }}
          text={"Add to cart"}
        />
      )}
      {fromCart && (
        <Button
          className="card-add-button"
          onClick={() => {
            saveCurrentProduct(product);
            openModal("Remove from cart");
          }}
          text={"X"}
        />
      )}
      <span
        className="card-star"
        onClick={() => {
          toggleFavorite(articul);
        }}
      >
        {star(isFavorite)}
      </span>
    </div>
  );
};

export default Card;

Card.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string,
    picPath: PropTypes.string,
    articul: PropTypes.string,
    color: PropTypes.string,
  }),
};
