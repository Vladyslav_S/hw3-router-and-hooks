import React from "react";
import "./Modal.scss";
import PropTypes from "prop-types";

const Modal = (props) => {
  const { header, closeButton, closeButtonOnClick, text, actions } = props;

  return (
    <div className="modal" onClick={closeButtonOnClick}>
      <div className="modal-content" onClick={(e) => e.stopPropagation()}>
        <header className="modal-content-header">
          <div>{header}</div>
          {closeButton && (
            <div onClick={closeButtonOnClick} className="close">
              &times;
            </div>
          )}
        </header>
        <div className="modal-content-body">
          <p className="modal-content-body-text">{text}</p>
          <div>{actions}</div>
        </div>
      </div>
    </div>
  );
};

export default Modal;

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.array,
};
