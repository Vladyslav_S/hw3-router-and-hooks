import React from "react";
import Card from "../Card/Card";
import "./CardList.scss";
import Cart from "../../routes/Cart/Cart";

const CardList = ({
  products,
  saveCurrentProduct,
  openModal,
  toggleFavorite,
  modalAction,
  favorites,
}) => {
  return (
    <div className="container">
      {products.map((product) => {
        return (
          <Card
            isFavorite={favorites.includes(product.articul)}
            modalAction={modalAction}
            key={product.articul}
            product={product}
            saveCurrentProduct={saveCurrentProduct}
            openModal={openModal}
            toggleFavorite={toggleFavorite}
            toCart
          />
        );
      })}
    </div>
  );
};

export default CardList;
