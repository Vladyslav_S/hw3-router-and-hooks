import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import CardList from "../components/CardList/CardList";
import Page404 from "./Page404/Page404";
import Favorites from "./Favorites/Favorites";
import Cart from "./Cart/Cart";

const Routes = ({
  products,
  saveCurrentProduct,
  openModal,
  toggleFavorite,
  modalAction,
  deleteCurrentProductFromCart,
  favorites,
}) => {
  return (
    <>
      <Switch>
        <Redirect exact from="/" to="/shop" />
        <Route
          exact
          path="/shop"
          render={() => {
            return (
              <CardList
                modalAction={modalAction}
                products={products}
                favorites={favorites}
                saveCurrentProduct={saveCurrentProduct}
                openModal={openModal}
                toggleFavorite={toggleFavorite}
                toCart
              />
            );
          }}
        />
        <Route
          exact
          path="/favorites"
          render={() => (
            <Favorites
              products={products}
              favorites={favorites}
              modalAction={modalAction}
              saveCurrentProduct={saveCurrentProduct}
              openModal={openModal}
              toggleFavorite={toggleFavorite}
            />
          )}
        />
        <Route
          exact
          path="/cart"
          render={() => (
            <Cart
              modalAction={modalAction}
              favorites={favorites}
              products={products}
              openModal={openModal}
              saveCurrentProduct={saveCurrentProduct}
              deleteCurrentProductFromCart={deleteCurrentProductFromCart}
              toggleFavorite={toggleFavorite}
            />
          )}
        />
        <Route path="*" component={Page404} />
      </Switch>
    </>
  );
};

export default Routes;
