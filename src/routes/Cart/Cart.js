import React from "react";
import "./Cart.scss";
import Card from "../../components/Card/Card";

const Cart = ({
  products,
  openModal,
  saveCurrentProduct,
  toggleFavorite,
  favorites,
}) => {
  if (!JSON.parse(localStorage.getItem("Cart"))) {
    return <div className="container">Cart is empty</div>;
  }

  const cart = JSON.parse(localStorage.getItem("Cart"));
  console.log(cart);
  const cartKeys = Object.keys(cart);
  const renderCart = products.filter((product) => {
    return cartKeys.includes(product.articul.toString());
  });

  console.log(renderCart);

  return (
    <div className="container">
      {renderCart.map((product) => {
        return (
          <Card
            isFavorite={favorites.includes(product.articul)}
            key={product.articul}
            product={product}
            saveCurrentProduct={saveCurrentProduct}
            openModal={openModal}
            toggleFavorite={toggleFavorite}
            fromCart
          />
        );
      })}
    </div>
  );
};

export default Cart;
