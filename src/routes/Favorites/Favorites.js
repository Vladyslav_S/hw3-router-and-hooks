import React from "react";
import Card from "../../components/Card/Card";

function Favorites({
  products,
  modalAction,
  saveCurrentProduct,
  openModal,
  toggleFavorite,
  favorites,
}) {
  if (!favorites.length) {
    return <div>Favorites are empty</div>;
  }

  return (
    <div className="container">
      {products
        .filter((product) => favorites.includes(product.articul))
        .map((product) => {
          return (
            <Card
              isFavorite
              modalAction={modalAction}
              key={product.articul}
              product={product}
              saveCurrentProduct={saveCurrentProduct}
              openModal={openModal}
              toggleFavorite={toggleFavorite}
              toCart
            />
          );
        })}
    </div>
  );
}

export default Favorites;
