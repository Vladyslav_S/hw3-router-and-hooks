import "./App.scss";

import React, { useEffect, useState } from "react";
import axios from "axios";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";
import Button from "./components/Buttom/Button";
import Sidebar from "./components/Sidebar/Sidebar";
import Routes from "./routes/Routes";

const getFavorites = () => {
  const LSFavorites = localStorage.getItem("favorite");
  return LSFavorites ? JSON.parse(LSFavorites) : [];
};

const App = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [products, setProducts] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [currentProduct, setCurrentProduct] = useState({});
  const [modalAction, setModalAction] = useState("Add to cart");

  const [favorites, setFavorites] = useState(getFavorites());

  const saveCurrentProduct = (prod) => {
    setCurrentProduct(prod);
  };

  const deleteCurrentProductFromCart = (prod) => {
    const cart = JSON.parse(localStorage.getItem("Cart"));

    delete cart[prod.articul];

    if (Object.keys(cart).length === 0) {
      localStorage.removeItem("Cart");
    } else {
      console.log(cart);
      localStorage.setItem("Cart", JSON.stringify(cart));
    }
    closeModal();
  };

  const openModal = (action) => {
    if (action === "Add to cart") {
      setModalAction("Add to cart");
    }
    if (action === "Remove from cart") {
      setModalAction("Remove from cart");
    }

    setShowModal(true);
  };

  const closeModal = () => {
    setShowModal(false);
  };

  const addToCart = (articul) => {
    if (localStorage.getItem("Cart")) {
      const cart = JSON.parse(localStorage.getItem("Cart"));

      // adding 1 more product to cart
      if (Object.keys(cart).includes(articul)) {
        cart[articul]++;
      } else {
        cart[articul] = 1;
      }

      localStorage.setItem("Cart", JSON.stringify(cart));
    } else {
      const cart = {};
      cart[articul] = 1;
      localStorage.setItem("Cart", JSON.stringify(cart));
    }
    closeModal();
  };

  const toggleFavorite = (articul) => {
    let newFavorites;
    if (favorites.includes(articul)) {
      newFavorites = favorites.filter((favorite) => {
        return favorite !== articul;
      });
    } else {
      newFavorites = [...favorites, articul];
    }
    setFavorites(newFavorites);
    localStorage.setItem("favorite", JSON.stringify(newFavorites));
  };

  useEffect(() => {
    axios("./products.json").then((res) => {
      res.data.forEach((card) => {
        card.isFavorite =
          localStorage.getItem("favorite") &&
          JSON.parse(localStorage.getItem("favorite")).includes(card.articul);
      });
      setProducts(res.data);
      setIsLoading(false);
    });
  }, []);

  if (isLoading) {
    return <div className="App">Loading... Don`t go anywhere</div>;
  }

  return (
    <div className="App">
      <Header>
        <Sidebar />
      </Header>
      <Routes
        products={products}
        favorites={favorites}
        saveCurrentProduct={saveCurrentProduct}
        openModal={openModal}
        toggleFavorite={toggleFavorite}
        modalAction={modalAction}
        deleteCurrentProductFromCart={deleteCurrentProductFromCart}
      />
      {showModal && (
        <Modal
          text={`${modalAction} "${currentProduct.name}"?`}
          closeButton={true}
          closeButtonOnClick={closeModal}
          header={modalAction}
          actions={[
            <Button
              key={1}
              onClick={() => {
                if (modalAction === "Add to cart") {
                  addToCart(currentProduct.articul);
                }
                if (modalAction === "Remove from cart") {
                  deleteCurrentProductFromCart(currentProduct);
                }
              }}
              className="modal-button"
              text="Ok"
            />,
            <Button
              key={2}
              onClick={closeModal}
              className="modal-button"
              text="Cancel"
            />,
          ]}
        />
      )}
    </div>
  );
};

export default App;
